from LightningModel import LightningModel
from SpectrogramCNN import SpectrogramCNN

def get_model():
  number_of_classes=6
  pytorch_model = SpectrogramCNN(num_classes=number_of_classes)
  print("Model loading...")
  return LightningModel.load_from_checkpoint(
    checkpoint_path="torch_model/movies_classifier.ckpt", 
    model=pytorch_model)