import cv2
from pydub import AudioSegment
import torch

def m4a_to_wav(path):
  m4a_file = f'{path}' # I have downloaded sample audio from this link https://getsamplefiles.com/sample-audio-files/m4a
  wav_filename = f'{path.replace(".m4a", "")}.wav'

  sound = AudioSegment.from_file(m4a_file, format='m4a')
  sound.export(wav_filename, format='wav')

def get_spectrogram_as_torch_tensor(path):
  img = cv2.imread(path)
  img = img[100:620, 177:1005]
  img = cv2.resize(img,(224, 224))
  img = torch.tensor(img).float().reshape(3, 224, 224)
  return img
