# 0 - Chicken run
# 1 - Finding Nemo
# 2 - IASIP
# 3 - None
# 4 - Shrek
# 5 - Zootopia

chicken_run = {
  "id": 0,
  "name": "Chicken run",
  "imageUrl": "https://variety.com/wp-content/uploads/2023/08/Chicken-Run-Dawn-of-the-Nugget.jpg"
}

finding_nemo = {
  "id": 1,
  "name": "Finding Nemo",
  "imageUrl": "https://lumiere-a.akamaihd.net/v1/images/pp_findingnemo_herobannermobile_19752_7810e507.jpeg?region=0,0,640,480"
}

iasip = {
  "id": 2,
  "name": "It's always sunny in Philadelphia",
  "imageUrl": "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/94E3B609C53985F2BD67F7EAB7BCFC40CFD57DA0741FAAE4963C2F746B10ECD0/scale?width=1200&aspectRatio=1.78&format=jpeg"
}

not_a_movie = {
  "id": 3,
  "name": "Couldn't find this movie, try again",
  "imageUrl": "https://cdn4.wpbeginner.com/wp-content/uploads/2013/04/wp404error.jpg"
}

shrek = {
  "id": 4,
  "name": "Shrek",
  "imageUrl": "https://variety.com/wp-content/uploads/2021/03/Shrek-Eddie-Murphy-1.jpg?w=1000"
}

zootopia = {
  "id": 5,
  "name": "Zootopia",
  "imageUrl": "https://images.seattletimes.com/wp-content/uploads/2016/03/63691e7e-dff7-11e5-a86a-581eb2be118e.jpg?d=780x326"
}

movies_dict = {
  0: chicken_run,
  1: finding_nemo,
  2: iasip,
  3: not_a_movie,
  4: shrek,
  5: zootopia
}