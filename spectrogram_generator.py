import torchaudio
import pandas as pd 
import librosa
import matplotlib.pyplot as plt


def generate_spectrogram(path):
  n_fft = 1024
  hop_length = 512

  spectrogram = torchaudio.transforms.Spectrogram(n_fft=n_fft,
                              hop_length=hop_length,
                              center=True,
                              pad_mode="reflect",
                              power=2.0)

  try:
    audio_sample_path = path
    print(audio_sample_path)
    waveform, sample_rate = torchaudio.load(audio_sample_path)
    spec = spectrogram(waveform)
    img_name = path.replace(".wav", "")
    fig, axs = plt.subplots(1, figsize=(16, 10))
    im = axs.imshow(librosa.power_to_db(spec[0]), origin='lower', aspect='auto')
    axs.axes.get_xaxis().set_visible(False)
    axs.axes.get_yaxis().set_visible(False)
    axs.set_frame_on(False)
    axs.set_xlabel(None)
    axs.set_ylabel(None)
    fig.savefig(f'spectrograms/{img_name.split("/")[1]}.png', 
                bbox_inches='tight', 
                pad_inches=0)
    plt.close()
  except:
    img_name = path.replace(".wav", "")
    print(f"Error at {img_name}")