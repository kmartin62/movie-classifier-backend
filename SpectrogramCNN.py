import lightning as L
import torch
import torch.nn.functional as F

class SpectrogramCNN(torch.nn.Module):
    def __init__(self, num_classes):
        super().__init__()
        self.cnn_layers = torch.nn.Sequential(
            torch.nn.Conv2d(3, 7, kernel_size=5),
            torch.nn.BatchNorm2d(7),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=2),
            
            torch.nn.Conv2d(7, 16, kernel_size=3),
            torch.nn.BatchNorm2d(16),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=2),
            
            torch.nn.Conv2d(16, 32, kernel_size=3),
            torch.nn.BatchNorm2d(32),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=2),
        )
        
        self.fc_layers = torch.nn.Sequential(
            torch.nn.Linear(21632, 12000),
            torch.nn.BatchNorm1d(12000),
            torch.nn.ReLU(),
            
            torch.nn.Linear(12000, 2000),
            torch.nn.BatchNorm1d(2000),
            torch.nn.ReLU(),
            
            torch.nn.Linear(2000, 500),
            torch.nn.BatchNorm1d(500),
            torch.nn.ReLU(),
            
            torch.nn.Linear(500, 20),
            torch.nn.BatchNorm1d(20),
            torch.nn.ReLU(),
            
            torch.nn.Linear(20, num_classes)
        )

    def forward(self, x):
        x = self.cnn_layers(x)
#         print(x.shape)
        x = torch.flatten(x, start_dim=1)
        logits = self.fc_layers(x)
        return logits
    

